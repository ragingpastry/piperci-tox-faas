import hashlib
from jinja2 import Template
import json
import tempfile
import requests
import os

from flask import g
from piperci.gman import client as gman_client
from piperci.gman.exceptions import TaskError
from piperci.artman import artman_client
from piperci.storeman.client import storage_client
from piperci.sri import generate_sri, hash_to_urlsafeb64

from .util import unzip_files, read_secrets, gman_activate
from .config import Config

gman_url = Config["gman"]["url"]
storage_url = Config["storage"]["url"]
function_name = Config["name"]
function_executor = f"{Config['name']}_executor"
executor_url = Config["executor_url"]


@gman_activate(status="received")
def handle(request):
    """
    Tox executor function definition. This handler function controls
    all functionality
    :param request: The request object from Flask
    This object is required to have the following JSON parameters:
    * run_id: The run_id of the task
    * thread_id: The thread_id, from gman, that this execution is forked from.
    * project: The project name of the run
    * configs: A list containing the configuration dictionaries for the run.
    * stage: The stage that is being run.
    * artifacts: A list of dictionaries containing information on the artifacts
    required for this run

    Workflow:
    1. Build Dockerfile from jinja2 template passing in `docker_image`
    2. Call docker function to build a docker image based on this Dockefile
      This will require creating a new request, passing in parent_id, thread_id, etc
      to the docker_gateway
    3. Track the status of the previous task until complete
    4. Call docker_function again, this time supplying the docker_image name and run_cmd
      as the tox_command
    5. Upload all logs and exit.

    :param request:
    :return:
    """
    run_id = request.get_json().get("run_id")
    stage = request.get_json()["stage"]
    configs = request.get_json()["configs"]
    artifacts = request.get_json()["artifacts"]
    project = request.get_json()["project"]
    task = g.task

    artifact = next(
        config.get("artifact", f"artifacts/{project}.zip") for config in configs
    )
    extra_artifacts = next(config.get("extra-artifacts") for config in configs)
    docker_image = next(config.get("docker-image") for config in configs)
    tox_args = next(config.get("tox-args") for config in configs)

    access_key = read_secrets().get("access_key")
    secret_key = read_secrets().get("secret_key")

    minio_client = storage_client(
        "minio", hostname=storage_url, access_key=access_key, secret_key=secret_key
    )

    with tempfile.TemporaryDirectory() as temp_directory:
        for art_name, art_data in artifacts.items():
            minio_client.download_file(
                art_data["artifact_uri"], os.path.join(temp_directory, art_name)
            )
            unzip_files(f"{temp_directory}/{art_name}", temp_directory)

        dockerfile = build_dockerfile(docker_image)
        sri_urlsafe = hash_to_urlsafeb64(generate_sri(dockerfile))
        if artman_client.check_artifact_exists(
            artman_url=gman_url, sri_urlsafe=sri_urlsafe
        ):
            dockerfile_uri = next(
                art.get("uri")
                for art in artman_client.get_artifact(
                    artman_url=gman_url, sri_urlsafe=sri_urlsafe
                    )
            )
        else:
            minio_client.upload_file(
                run_id, f"artifacts/{stage}/{os.path.basename(dockerfile)}", dockerfile
            )
            dockerfile_uri = (
                f"minio://{storage_url}/{run_id}/artifacts/{stage}/{os.path.basename(dockerfile)}"
            )
            artman_client.post_artifact(
                task_id=task["task"]["task_id"],
                artman_url=gman_url,
                uri=dockerfile_uri,
                caller="docker_executor",
                sri=str(generate_sri(dockerfile)),
            )
        # We will need to call the docker faas with the extra artifacts option
        # To grab the project and include the Dockerfile. then the docker faas
        # will unzip the project in a structure like this:
        #  tmpdir
        #    - project_dir
        #    - dockerfile_dir
        # and build using `cd project_dir && docker build -f dockerfile_dir/Dockerfile`
        # Create artifacts dict (like in picli)
        # Call docker function w/ error handling
        # Once complete we can call docker function again
        # with original project artifact and the docker-image + run_cmd of our
        # tox-command.
        configs = [
            {
                "resource": "docker",
                "docker-file": dockerfile_uri,
                "run-cmd": tox_args,
                "build-cmds": {
                    "target": "builder",
                    "cache_from": [f"{project}-tox-faas:builder"],
                }
            }
        ]
        call_docker_function(
            run_id=run_id,
            project=project,
            artifacts=artifacts,
            parent_id=task['task']['task_id'],
            thread_id=task['task']['thread_id'],
            configs=configs,
            stage=stage,
        )


def build_dockerfile(docker_image):
    template = Template(open("function/Dockerfile.j2").read())
    rendered_template = template.render(docker_image=docker_image)
    with open("Dockerfile", "w") as f:
        f.write(rendered_template)
    return os.path.abspath("Dockerfile")


def call_docker_function(*, run_id, thread_id, project, artifacts, parent_id, configs, stage):
    headers = {"Content-Type": "application/json"}
    task_data = {
        "run_id": run_id,
        "project": project,
        "artifacts": artifacts,
        "parent_id": parent_id,
        "configs": configs,
        "thread_id": thread_id,
        "stage": stage,
    }

    r = requests.post(
        "http://172.17.0.1:8080/function/piperci-docker-gateway",
        data=json.dumps(task_data),
        headers=headers,
    )
    r.raise_for_status()
    return r.json()
